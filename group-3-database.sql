-- MySQL dump 10.13  Distrib 8.0.14, for macos10.14 (x86_64)
--
-- Host: localhost    Database: medical_records
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `records`
--

DROP TABLE IF EXISTS `records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `medicalID` varchar(9) NOT NULL,
  `birthdate` varchar(45) NOT NULL,
  `address` varchar(255) NOT NULL,
  `medications` varchar(255) NOT NULL,
  `allergies` varchar(255) NOT NULL,
  `images` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `patientSince` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `doctorID` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `records`
--

LOCK TABLES `records` WRITE;
/*!40000 ALTER TABLE `records` DISABLE KEYS */;
INSERT INTO `records` VALUES (27,'Tyler Lalonde','123456789','2002-12-17','Somewhere in Kelowna...','Vitamin D (don\'t get enough sunlight and Myasthenia)','None',NULL,NULL,'2022-08-14 05:23:25',1),(39,'Stanley Tucci','861220996','1955-07-06','55 Park Avenue West\nNew York, NY\n77419','Xanax','Cats',NULL,'Patient is a great actor.','2022-08-15 18:11:12',2),(55,'Tyler Patient 1','123456789','1980-11-05','111 Test Street\nAnywhere, BC\nR5D 4R5','Advil','Cats',NULL,NULL,'2022-08-15 19:09:59',2),(56,'Tyler Patient 2','098765431','2000-09-09','55 East Road\nToronto, ON\nM5W 1E6','Tylenol','Pollen',NULL,'Test note for Patient 2','2022-08-15 19:10:56',2),(58,'Wade Patient 2','012484713','1999-08-04','33 Main Street\nCalgary, AB\nT7Y 4R6','Steroids','Peanuts',NULL,NULL,'2022-08-15 19:13:30',1),(59,'Wade Patient 3 - Assigned by Admin','123456789','1976-09-20','22 East Street\nEdmonton, AB\nH7R 5T8','Candy','Exercise',NULL,NULL,'2022-08-15 19:14:57',1),(60,'Tyler Patient 3 - Assigned by Admin	','345678021','2001-08-20','1234 West Road\nRegina, SK\nB7T 8Y5','Fentanyl','Fish',NULL,NULL,'2022-08-15 19:16:24',2),(61,'Wade Patient 4','123456789','1989-04-04','1234 Street\nAnywhere, Canada\nT5T 6Y6','Everything','Nothing',NULL,'Test note for patient 4','2022-08-15 19:42:17',1),(62,'Tyler Patient 4','123099765','2002-05-05','444 Main Street\nVictoria, BC\nV7T 5R3','Antibiotics','Cats',NULL,'Note for Patient 4','2022-08-15 19:44:05',2),(63,'Wade Patient 5 - Assigned by Admin','987654322','2009-11-22','987 Adler Avenue\nSaskatoon, SK\nH7T 5R8','Heart medication','Penicillin',NULL,'Test note','2022-08-15 19:45:52',1),(75,'Bob Jones','123456789','1990-05-05','123 Main Street\nVancouver, BC\nV9E 1S6','Tylenol','Pollen',NULL,NULL,'2022-08-19 18:33:17',2),(76,'Steven Waters','987123654','2022-02-09','22 West Lane\nCalgary, AB\nT5E 1D6','Antibiotics','Cats',NULL,NULL,'2022-08-19 18:34:05',1),(77,'Darren McEwan','097127643','1966-08-08','551 Portage Avenue\nWinnipeg, MB\nR2S 7U6','Advil','Peanuts',NULL,NULL,'2022-08-19 18:37:16',1);
/*!40000 ALTER TABLE `records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registered_users`
--

DROP TABLE IF EXISTS `registered_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `registered_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `password_UNIQUE` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registered_users`
--

LOCK TABLES `registered_users` WRITE;
/*!40000 ALTER TABLE `registered_users` DISABLE KEYS */;
INSERT INTO `registered_users` VALUES (1,'wade','password'),(2,'tyler','password2'),(3,'admin','adminpass');
/*!40000 ALTER TABLE `registered_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-19 18:55:08
